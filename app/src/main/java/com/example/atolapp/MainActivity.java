package com.example.atolapp;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;

import android.Manifest;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

import com.squareup.timessquare.CalendarPickerView;

import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.mail.Session;

public class MainActivity extends AppCompatActivity {

    // Variaveis Globais para verificar permissao

    private static final int PERMISSION_STORAGE_CODE = 1000;
    private static final String TAG = "MainActivity";
    private String[] FilePathStrings;
    private String[] FileNameStrings;
    private File[] listFile;
    File file;
    ArrayList<String> pathHistory;
    String lastDirectory;
    int count = 0;
    int exist = 0;

    // Variaveis Mail Sender

    Session session = null;
    ProgressDialog pdialog = null;

    // Variaveis para o xlsx
    ArrayList<Date> dateSelect = new ArrayList<Date>();
    public static ArrayList<dataOrganizer> dados = new ArrayList<dataOrganizer>();
    String remetentes = "", nome, nomeLinha, abv;
    int totTorres, countRows = 0;


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActionBar actionBar = getSupportActionBar();

        checkFilePermissions();

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(new Date());
        cal1.set(Calendar.DAY_OF_MONTH, cal1.get(Calendar.DAY_OF_MONTH) + 3);


        if (dados.isEmpty())
            dateSelect = getDates(dados, countRows);
        else
            Log.d(TAG, "Dados ja preenchido\n");

        Log.d(TAG, "DADOS SIZES: " + dados.size());


        if (dateSelect != null) {

            final CalendarPickerView calendar = findViewById(R.id.calendar);
            if (!nome.isEmpty())
                actionBar.setTitle(nome.substring(0, nome.indexOf(" ")) + " - RDO v.1.5");
            calendar.init(dateSelect.get(0), cal1.getTime()).withHighlightedDates(dateSelect).withSelectedDate(dateSelect.get(0));
            calendar.setOnDateSelectedListener(new CalendarPickerView.OnDateSelectedListener() {
                @Override
                public void onDateSelected(Date date) {

                    int i, index = 0, inList = 0;

                    for (i = 0; i < dateSelect.size(); i++) {
                        if (dateSelect.get(i).equals(date)) {
                            index = i;
                            inList = 1;
                            break;
                        }
                    }

                    if (inList == 1) {
                        Intent popUp = new Intent(MainActivity.this, Pop.class);
                        popUp.putExtra("date", dados.get(index).date);
                        popUp.putExtra("atividade", dados.get(index).atividade);
                        popUp.putExtra("qntTorres", dados.get(index).qntTorres);
                        popUp.putExtra("executado", dados.get(index).executado);
                        popUp.putExtra("programaDiaSeguinte", dados.get(index).progExec);
                        popUp.putExtra("condClimatica", dados.get(index).condClimatica);
                        popUp.putExtra("obs", dados.get(index).obs);
                        popUp.putExtra("index", index);
                        popUp.putExtra("edit", 1);
                        startActivityForResult(popUp, 1);
                    } else {
                        SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
                        Intent popUp = new Intent(MainActivity.this, Pop.class);
                        popUp.putExtra("date", sd.format(calendar.getSelectedDate()));
                        popUp.putExtra("atividade", dados.get(index).atividade);
                        popUp.putExtra("qntTorres", dados.get(index).qntTorres);
                        popUp.putExtra("executado", dados.get(index).executado);
                        popUp.putExtra("programaDiaSeguinte", dados.get(index).progExec);
                        popUp.putExtra("condClimatica", dados.get(index).condClimatica);
                        popUp.putExtra("obs", dados.get(index).obs);
                        popUp.putExtra("index", dados.size());
                        popUp.putExtra("edit", 0);
                        startActivityForResult(popUp, 0);
                    }

                }

                @Override
                public void onDateUnselected(Date date) {

                }
            });

        } else {

            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            if (nome != null && exist != 0)
                actionBar.setTitle(nome.substring(0, nome.indexOf(" ")) + " - RDO v.1.5");
            // substract 7 days
            // If we give 7 there it will give 8 days back
            cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH) - 7);

            // convert to date
            Date date = cal.getTime();
            SimpleDateFormat sm = new SimpleDateFormat("dd/MM/yy");
            Log.d(TAG, "date = " + sm.format(date));
            final CalendarPickerView calendar = findViewById(R.id.calendar);
            calendar.init(date, cal1.getTime()).withSelectedDate(new Date());
            calendar.setOnDateSelectedListener(new CalendarPickerView.OnDateSelectedListener() {
                @Override
                public void onDateSelected(Date date) {

                    int i, index = 0, inList = 0;

                    if (dateSelect != null)
                        for (i = 0; i < dateSelect.size(); i++) {
                            if (dateSelect.get(i).equals(date)) {
                                index = i;
                                inList = 1;
                                break;
                            }
                        }

                    if (inList == 1) {
                        Intent popUp = new Intent(MainActivity.this, Pop.class);
                        popUp.putExtra("date", dados.get(index).date);
                        popUp.putExtra("atividade", dados.get(index).atividade);
                        popUp.putExtra("qntTorres", dados.get(index).qntTorres);
                        popUp.putExtra("executado", dados.get(index).executado);
                        popUp.putExtra("programaDiaSeguinte", dados.get(index).progExec);
                        popUp.putExtra("condClimatica", dados.get(index).condClimatica);
                        popUp.putExtra("obs", dados.get(index).obs);
                        popUp.putExtra("index", index);
                        popUp.putExtra("edit", 1);
                        startActivityForResult(popUp, 1);
                    } else {
                        SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
                        Intent popUp = new Intent(MainActivity.this, Pop.class);
                        popUp.putExtra("date", sd.format(calendar.getSelectedDate()));
                        popUp.putExtra("atividade", "");
                        popUp.putExtra("qntTorres", 0);
                        popUp.putExtra("executado", "");
                        popUp.putExtra("programaDiaSeguinte", "");
                        popUp.putExtra("condClimatica", "");
                        popUp.putExtra("obs", "");
                        popUp.putExtra("index", dados.size());
                        popUp.putExtra("edit", 0);
                        startActivityForResult(popUp, 0);
                    }

                }

                @Override
                public void onDateUnselected(Date date) {

                }
            });
        }

    }

    private ArrayList<Date> getDates(ArrayList<dataOrganizer> dados, int countRows) {


        String filePath = "/sdcard/Download/RELATORIO-DIARIO-DE-OBRA-ATOL.xlsx";
        String value, value2;
        //declaração de input file
        ArrayList<Date> dates = new ArrayList<Date>();
        File inputFile = new File(filePath);

        try {

            InputStream inputStream = new FileInputStream(inputFile);
            XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
            XSSFSheet sheet = workbook.getSheetAt(0);
            XSSFSheet sheetEmail = workbook.getSheetAt(1);
            FormulaEvaluator formulaEvaluator = workbook.getCreationHelper().createFormulaEvaluator();

            dataOrganizer dataAuxiliar = new dataOrganizer("", "", 0, "", "", "", "");
            int rowsCount = sheet.getPhysicalNumberOfRows();

            for (int i = 10; i < rowsCount; i += 2) {
                Row row = sheet.getRow(i);
                value = getCellAsString(row, 0, formulaEvaluator);

                if (value.equals("")) {
                    break;
                } else {
                    int cellsCount = row.getPhysicalNumberOfCells();

                    for (int j = 0; j < 7; j++) {
                        if (j > 7) {
                            Log.e(TAG, "readExcelData: ERROR. Excel File Format is incorrect! ");
                            toastMessage("ERROR: Excel File Format is incorrect!");
                            break;
                        } else {
                            Row row2 = sheet.getRow(i + 1);
                            value = getCellAsString(row, j, formulaEvaluator);
                            value2 = getCellAsString(row2, j, formulaEvaluator);

                            switch (j) {
                                case 0: {
                                    if (!value.equals("")) {
                                        dataAuxiliar.date = value;
                                    }
                                    break;
                                }
                                case 1: {
                                    dataAuxiliar.atividade = value + " " + value2;
                                    break;
                                }
                                case 2: {
                                    if (!value.isEmpty())
                                        dataAuxiliar.qntTorres = Integer.parseInt(value.substring(0, value.indexOf(".")));
                                    else
                                        dataAuxiliar.qntTorres = 0;


                                    break;
                                }
                                case 3: {
                                    if (!value.equals(""))
                                        dataAuxiliar.executado = value + " " + value2;
                                    else
                                        dataAuxiliar.executado = " ";
                                    break;
                                }
                                case 4: {
                                    dataAuxiliar.progExec = value + " " + value2;
                                    break;
                                }
                                case 5: {
                                    dataAuxiliar.condClimatica = value;
                                    break;
                                }
                                case 6: {
                                    if (!value.equals(""))
                                        dataAuxiliar.obs = value + " " + value2;
                                    else
                                        dataAuxiliar.obs = " ";

                                    break;
                                }
                                default: {
                                    break;
                                }

                            }

                        }
                    }

                    Log.d(TAG, "OBS => " + dataAuxiliar.obs);
                    dados.add(new dataOrganizer(dataAuxiliar.date, dataAuxiliar.atividade, dataAuxiliar.qntTorres, dataAuxiliar.executado,
                            dataAuxiliar.progExec, dataAuxiliar.condClimatica, dataAuxiliar.obs));

                }

            }

            countRows = rowsCount;
            // Pega nome do topografo
            final Row rowTopografo = sheet.getRow(7);
            value = getCellAsString(rowTopografo, 0, formulaEvaluator);
            nome = value;

            // Pega remetentes para email
            int qntEmail = sheetEmail.getPhysicalNumberOfRows();
            Row rowEmail = sheetEmail.getRow(0);


            ArrayList<String> rm = new ArrayList<String>();
            for (int j = 0; j < qntEmail; j++)
                rm.add(getCellAsString(sheetEmail.getRow(j), 0, formulaEvaluator));

            remetentes = "";
            for (int j = 0; j < rm.size(); j++) {
                remetentes += rm.get(j);
                if (rm.size() > 1 && j < rm.size() - 1)
                    remetentes += ", ";
            }


            Log.d(TAG, "EMAILS: " + remetentes);

            //Abreviação
            Row rowAb = sheet.getRow(6);
            abv = getCellAsString(rowAb, 0, formulaEvaluator);


            // Nome da linha
            Row row = sheet.getRow(2);
            value = getCellAsString(row, 2, formulaEvaluator);
            nomeLinha = value.substring(value.indexOf(":") + 2, value.length());
            Log.d(TAG, "Nome linha: " + nomeLinha);

            for (int i = 0; i < dados.size(); i++) {
                Date initDate = new SimpleDateFormat("dd/MM/yyyy").parse(dados.get(i).date);
                dates.add(initDate);
            }

            exist = 1;

        } catch (ParseException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {


            AlertDialog.Builder builderSingle = new AlertDialog.Builder(MainActivity.this);
            builderSingle.setTitle("Selecione o seu nome:  ");

            final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.select_dialog_singlechoice);
            arrayAdapter.add("Aldeoni");
            arrayAdapter.add("Andre");
            arrayAdapter.add("Carlos");
            arrayAdapter.add("Clesio");
            arrayAdapter.add("Leandro");
            arrayAdapter.add("Valmir");


            builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String strName = arrayAdapter.getItem(which);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                                PackageManager.PERMISSION_DENIED) {

                            String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
                            requestPermissions(permissions, PERMISSION_STORAGE_CODE);

                        } else {

                            downloadFile(strName);
                        }

                    } else {
                        downloadFile(strName);
                    }


                }
            });
            builderSingle.show();


            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (dates.isEmpty()) return null;

        return dates;
    }

    public void downloadFile(String nome) {


        Uri uri = Uri.parse("http://www.atollaser.com.br/campo/rdo/" + nome + "/RELATORIO-DIARIO-DE-OBRA-ATOL.xlsx");
        Log.d(TAG, uri.toString());

        deleteCache(getApplicationContext());
        new DownloadFile().execute(uri.toString());



    }


    public static void deleteCache(Context context) {
     try {
        File dir = context.getCacheDir();
        deleteDir(dir);
     } catch (Exception e) {}
    }

    public static boolean deleteDir(File dir) {
     if (dir != null && dir.isDirectory()) {
         String[] children = dir.list();
         for (int i = 0; i < children.length; i++) {
            boolean success = deleteDir(new File(dir, children[i]));
            if (!success) {
                return false;
            }
         }

         return dir.delete();
     } else if(dir!= null && dir.isFile()) {
         return dir.delete();
     } else {
        return false;
     }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btnEnvia: {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage("Enviar email ?")
                        .setCancelable(false)
                        .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                new Thread(new Runnable() {
                                    public void run() {
                                        try {
                                            Date date = new Date();
                                            SimpleDateFormat dateSelect = new SimpleDateFormat("dd-MM-yy");
                                            GMailSender sender = new GMailSender(
                                                    "RDOAtol@gmail.com",
                                                    "rdoAtol@Atol_951");

                                            sender.addAttachment("/sdcard/Downloads/RELATORIO-DIARIO-DE-OBRA-ATOL.xlsx", nome, dateSelect.format(date), abv);
                                            sender.sendMail("RDO-ATOL-" + abv + "-" + nome.substring(0, nome.indexOf(" ")) + "-" + dateSelect.format(date),
                                                    "Prezados,\n\nSegue em anexo o Relatório Diário de Obras da " + nomeLinha + ", atualizado até o\ndia " + dateSelect.format(date) + " pelo Topógrafo " + nome + "\n\nAtenciosamente,\n\n" + nome + "\nAtol Topografia Laser Ltda.",
                                                    "RDOAtol@gmail.com",
                                                    remetentes + ", flavia@atollaser.com.br");


                                        } catch (Exception e) {
                                            Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
                                        }
                                    }
                                }).start();

                                final Handler h = new Handler();
                                h.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        AlertDialog.Builder builder2 = new AlertDialog.Builder(MainActivity.this);
                                        builder2.setTitle("Email enviado com sucesso");
                                        builder2.setMessage("\nRemetentes: " + remetentes);
                                        AlertDialog alert2 = builder2.create();
                                        alert2.show();

                                    }
                                }, 4000);

                            }
                        })
                        .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();
                return true;
            }
            // User chose the "Settings" item, show the app settings UI...
            case R.id.ExcluiTab: {

                String filePath = "/sdcard/Download/RELATORIO-DIARIO-DE-OBRA-ATOL.xlsx";
                final File file = new File(filePath);

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage("Tem certeza ?")
                        .setCancelable(false)
                        .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                file.delete();

                                String firstName = nome.substring(0,nome.indexOf(" "));
                                String selectName = null;
                                switch (firstName){
                                    case "Clésio":{
                                        selectName = "Clesio";
                                        break;
                                    }
                                     case "Aldeoni":{
                                        selectName = "Aldeoni";
                                        break;
                                    }
                                     case "André":{
                                        selectName = "Andre";
                                        break;
                                    }
                                     case "Carlos":{
                                        selectName = "Carlos";
                                        break;
                                    }
                                     case "Leandro":{
                                        selectName = "Leandro";
                                        break;
                                    }
                                     case "Valmir":{
                                        selectName = "Valmir";
                                        break;
                                    }
                                    default:{
                                        AlertDialog.Builder builder2 = new AlertDialog.Builder(MainActivity.this);
                                        builder2.setTitle("Erro para obtenção de tabela !!");
                                        AlertDialog alert2 = builder2.create();
                                        alert2.show();
                                        break;
                                    }


                                }

                                downloadFile(selectName);

                                AlertDialog.Builder builderInner = new AlertDialog.Builder(MainActivity.this);
                                builderInner.setMessage("Tabela obtida com sucesso !\n\n" +
                                                            "O aplicativo será reinicializado !!!");
                                builderInner.setTitle("Tabela atualizada !!");
                                builderInner.setPositiveButton("Ok", new DialogInterface.OnClickListener() {


                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    dialog.dismiss();

                                    try {
                                        new Thread().sleep(7000);
                                    } catch (InterruptedException e) {
                                    e.printStackTrace();
                                    }

                                    Intent mStartActivity = new Intent(getApplicationContext(), SplashScreen.class);


                                    //Realiza o agendamento da intent de abrir o aplicativo:
                                    //No caso abaixo o aplicativo vai ser reaberto daqui 500ms (System.currentTimeMillis() + 500);
                                    PendingIntent mPendingIntent = PendingIntent.getActivity(getApplicationContext(), 123456, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
                                    AlarmManager mgr = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
                                    mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 3000, mPendingIntent);

                                    //Mata todos processos associados a este aplicativo.
                                    android.os.Process.killProcess(android.os.Process.myPid());

                                    //Fecha o aplicativo.
                                    System.exit(1); dialog.dismiss();
                                }
                                });
                                builderInner.show();// SINALIZAR QUE FOI EXCLUIDO OU QUE FOI ATUALIZADO

                            }
                        })
                        .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();
                return true;
            }
            default: {
                // If we got here, the user's actio Log.d(TAG,"apenas testando");
                return super.onOptionsItemSelected(item);
            }

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);

        return true;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 2) {

            CalendarPickerView calendar = findViewById(R.id.calendar);
            Date initDate = null;
            for (int i = 0; i < MainActivity.dados.size(); i++) {
                try {
                    initDate = new SimpleDateFormat("dd/MM/yyyy").parse(dados.get(i).date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if (dateSelect != null) {
                    if (dateSelect.indexOf(initDate) == -1) {
                        dateSelect.add(initDate);
                    }
                } else
                    dateSelect = getDates(dados, countRows);

            }

            calendar.highlightDates(dateSelect);
        }

    }

    // Pegar valor da celula escolhida
    public String getCellAsString(Row row, int c, FormulaEvaluator formulaEvaluator) {
        String value = "";
        try {
            Cell cell = row.getCell(c);
            CellValue cellValue = formulaEvaluator.evaluate(cell);
            switch (cellValue.getCellType()) {
                case Cell.CELL_TYPE_BOOLEAN:
                    value = "" + cellValue.getBooleanValue();
                    break;
                case Cell.CELL_TYPE_NUMERIC:
                    double numericValue = cellValue.getNumberValue();
                    if (HSSFDateUtil.isCellDateFormatted(cell)) {
                        double date = cellValue.getNumberValue();
                        SimpleDateFormat formatter =
                                new SimpleDateFormat("dd/MM/yyyy");
                        value = formatter.format(HSSFDateUtil.getJavaDate(date));
                    } else {
                        value = "" + numericValue;
                    }
                    break;
                case Cell.CELL_TYPE_STRING:
                    value = "" + cellValue.getStringValue();
                    break;
                default:
            }
        } catch (NullPointerException e) {

            Log.e(TAG, "getCellAsString: NullPointerException: " + e.getMessage());
        }
        return value;
    }

    // Permissoes para o aplicativo

    private void checkInternalStorage() {
        Log.d(TAG, "checkInternalStorage: Started.");
        try {
            if (!Environment.getExternalStorageState().equals(
                    Environment.MEDIA_MOUNTED)) {
                toastMessage("No SD card found.");
            } else {
                // Locate the image folder in your SD Car;d
                file = new File(pathHistory.get(count));
                Log.d(TAG, "checkInternalStorage: directory path: " + pathHistory.get(count));
            }

            listFile = file.listFiles();

            // Create a String array for FilePathStrings
            FilePathStrings = new String[listFile.length];

            // Create a String array for FileNameStrings
            FileNameStrings = new String[listFile.length];

            for (int i = 0; i < listFile.length; i++) {
                // Get the path of the image file
                FilePathStrings[i] = listFile[i].getAbsolutePath();
                // Get the name image file
                FileNameStrings[i] = listFile[i].getName();
            }

            for (int i = 0; i < listFile.length; i++) {
                Log.d("Files", "FileName:" + listFile[i].getName());
            }

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, FilePathStrings);

        } catch (NullPointerException e) {
            Log.e(TAG, "checkInternalStorage: NULLPOINTEREXCEPTION " + e.getMessage());
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void checkFilePermissions() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            int permissionCheck = this.checkSelfPermission("Manifest.permission.READ_EXTERNAL_STORAGE");
            permissionCheck += this.checkSelfPermission("Manifest.permission.WRITE_EXTERNAL _STORAGE");
            if (permissionCheck != 0) {

                this.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 1001); //Any number
            }
        } else {
            Log.d(TAG, "checkBTPermissions: No need to check permissions. SDK version < LOLLIPOP.");
        }
    }

    private void toastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }



    private class DownloadFile extends AsyncTask<String, String, String> {

        private static final String TAG = "MainActivity";
        private ProgressDialog progressDialog;
        private String fileName;
        private String folder;
        private boolean isDownloaded;

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.progressDialog = new ProgressDialog(MainActivity.this);
            this.progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            this.progressDialog.setCancelable(false);
            this.progressDialog.show();
        }


        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection connection = url.openConnection();
                connection.connect();
                // getting file length
                int lengthOfFile = connection.getContentLength();


                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                String timestamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());

                //Extract file name from URL
                fileName = f_url[0].substring(f_url[0].lastIndexOf('/') + 1, f_url[0].length());

                //External directory path to save file
                File dir = Environment.getDownloadCacheDirectory();
                Log.d(TAG,"DIR =====> "+dir.toString());



                // Delete local cache dir (ignoring any errors):
                FileUtils.deleteQuietly(Environment.getDownloadCacheDirectory());
                folder = Environment.getExternalStorageDirectory()+"/Download/";

                //Create androiddeft folder if it does not exist
                File directory = new File(folder);

                if (!directory.exists()) {
                    directory.mkdirs();
                }

                // Output stream to write file
                OutputStream output = new FileOutputStream(folder + fileName);

                byte data[] = new byte[1000000];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lengthOfFile));
                    Log.d(TAG, "Progress: " + (int) ((total * 100) / lengthOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();
                return "Downloaded at: " + folder + fileName;

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return "Something went wrong";
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            progressDialog.setProgress(Integer.parseInt(progress[0]));
        }



        @Override
        protected void onPostExecute(String message) {
            // dismiss the dialog after the file was downloaded
            this.progressDialog.dismiss();

            Toast.makeText(getApplicationContext(),
                    message, Toast.LENGTH_LONG).show();

            AlertDialog.Builder builderInner = new AlertDialog.Builder(MainActivity.this);
                    builderInner.setMessage("Tabela obtida com sucesso !\n\n" +
                            "O aplicativo será reinicializado !!!");
                    builderInner.setTitle("Download da Tabela");
                    builderInner.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.dismiss();
                            try {
                                new Thread().sleep(2000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            Intent mStartActivity = new Intent(getApplicationContext(), SplashScreen.class);

                            //Realiza o agendamento da intent de abrir o aplicativo:
                            //No caso abaixo o aplicativo vai ser reaberto daqui 500ms (System.currentTimeMillis() + 500);
                            PendingIntent mPendingIntent = PendingIntent.getActivity(getApplicationContext(), 123456, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
                            AlarmManager mgr = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
                            mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 3000, mPendingIntent);

                            //Mata todos processos associados a este aplicativo.
                            android.os.Process.killProcess(android.os.Process.myPid());
                            //Fecha o aplicativo.
                            System.exit(1); dialog.dismiss();
                        }
                    });
                    builderInner.show();

        }


    }
}

