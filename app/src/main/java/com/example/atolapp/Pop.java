package com.example.atolapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Pop extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    int index, edit, qntTorres,goingEdit = 0;
    String date, executado, programaDiaSeguinte, obs, atividade, condClimatica;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.popwindows);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Spinner spinnerAtividade = findViewById(R.id.spinnerAtividade);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.atividade, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerAtividade.setAdapter(adapter);

        Spinner spinnerClima = findViewById(R.id.spinnerClima);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this, R.array.clima, android.R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerClima.setAdapter(adapter2);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        Intent i = getIntent();
        Bundle extra = i.getExtras();

        if (extra.containsKey("date"))
            date = i.getStringExtra("date");

        if (extra.containsKey("atividade"))
            atividade = i.getStringExtra("atividade");

        if (extra.containsKey("qntTorres"))
            qntTorres = i.getIntExtra("qntTorres", 0);

        if (extra.containsKey("executado"))
            executado = i.getStringExtra("executado");

        if (extra.containsKey("programaDiaSeguinte"))
            programaDiaSeguinte = i.getStringExtra("programaDiaSeguinte");

        if (extra.containsKey("obs"))
            obs = i.getStringExtra("obs");

        if (extra.containsKey("index"))
            index = i.getIntExtra("index", 0);

        if (extra.containsKey("condClimatica"))
            condClimatica = i.getStringExtra("condClimatica");

        if (extra.containsKey("edit"))
            edit = i.getIntExtra("edit", 0);

        final Button btnSalvar = findViewById(R.id.btnSalvar);
        btnSalvar.setVisibility(View.INVISIBLE);
        btnSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView textQnt = findViewById(R.id.textQnt);
                TextView textExec = findViewById(R.id.textExec);
                TextView textProg = findViewById(R.id.textProg);
                TextView textObs = findViewById(R.id.textObs);
                Spinner textAtividade = findViewById(R.id.spinnerAtividade);
                Spinner textClima = findViewById(R.id.spinnerClima);

                if ( !textQnt.getText().toString().isEmpty() && !textAtividade.getSelectedItem().equals("--Escolha uma atividade--") &&
                        !textClima.getSelectedItem().equals("--Escolha o clima--") && !textExec.getText().toString().isEmpty()
                        && !textProg.getText().toString().isEmpty()) {
                    final dataOrganizer dado = new dataOrganizer(date, textAtividade.getSelectedItem().toString(),
                            Integer.parseInt(textQnt.getText().toString()), textExec.getText().toString(),
                            textProg.getText().toString(), textClima.getSelectedItem().toString(),
                            textObs.getText().toString());

                    AlertDialog.Builder builder2 = new AlertDialog.Builder(Pop.this);
                    builder2.setMessage("Salvar Dados ? ").setCancelable(false)
                            .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                      if (goingEdit == 1)
                                      {
                                          MainActivity.dados.set(index,dado);
                                          atualizaDados(index,1);
                                          setResult(1);
                                      }
                                      else{
                                        MainActivity.dados.add(dado);
                                        atualizaDados(0,0);
                                        setResult(2);
                                    }
                                    Pop.this.finish();
                                }
                            })
                            .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert2 = builder2.create();
                    alert2.show();
                }
                else
                {

                    boolean qnt, ativ,clima,exec,prog;
                    qnt = textQnt.getText().toString().isEmpty() ;
                    ativ = textAtividade.getSelectedItem().equals("--Escolha uma atividade--");
                    clima = textClima.getSelectedItem().equals("--Escolha o clima--") ;
                    exec = textExec.getText().toString().isEmpty();
                    prog = textProg.getText().toString().isEmpty() ;

                    if ( ativ )
                    {
                        AlertDialog.Builder builder2 = new AlertDialog.Builder(Pop.this);
                        builder2.setTitle("Campo Incompleto !!");
                        builder2.setMessage("Atividade Principal não selecionado !");
                        AlertDialog alert2 = builder2.create();
                        alert2.show();

                    }
                    else
                    {
                        if ( clima ){
                            AlertDialog.Builder builder2 = new AlertDialog.Builder(Pop.this);
                            builder2.setTitle("Campo Incompleto !!");
                            builder2.setMessage("Clima não selecionado !");
                            AlertDialog alert2 = builder2.create();
                            alert2.show();
                        }
                        else
                        {
                            if ( qnt ){
                                AlertDialog.Builder builder2 = new AlertDialog.Builder(Pop.this);
                                builder2.setTitle("Campo Incompleto !!");
                                builder2.setMessage("Quantidade de torres não selecionado !");
                                AlertDialog alert2 = builder2.create();
                                alert2.show();
                            }
                            else
                            {
                                if (exec){
                                    AlertDialog.Builder builder2 = new AlertDialog.Builder(Pop.this);
                                    builder2.setTitle("Campo Incompleto !!");
                                    builder2.setMessage("Torres Executados não selecionado !");
                                    AlertDialog alert2 = builder2.create();
                                    alert2.show();
                                }
                                else
                                {
                                    if ( prog){
                                        AlertDialog.Builder builder2 = new AlertDialog.Builder(Pop.this);
                                        builder2.setTitle("Campo Incompleto !!");
                                        builder2.setMessage("Programação do dia seguinte  não selecionado !");
                                        AlertDialog alert2 = builder2.create();
                                        alert2.show();
                                    }

                                }
                            }
                        }
                    }


                }


            }

        });


        final Button btnEdit = findViewById(R.id.btnEdit);
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                TextView textQnt = findViewById(R.id.textQnt);
                TextView textExec = findViewById(R.id.textExec);
                TextView textProg = findViewById(R.id.textProg);
                TextView textObs = findViewById(R.id.textObs);
                Spinner textAtividade = findViewById(R.id.spinnerAtividade);
                Spinner textClima = findViewById(R.id.spinnerClima);

                btnEdit.setVisibility(View.INVISIBLE);
                btnSalvar.setVisibility(View.VISIBLE);

                textAtividade.setEnabled(true);
                textClima.setEnabled(true);
                textQnt.setEnabled(true);
                textExec.setEnabled(true);
                textProg.setEnabled(true);
                textObs.setEnabled(true);

                goingEdit = 1;

            }
        });

        if (edit == 1) {

            int indexAtividade = -1, indexClima = -1;
            TextView textDate = findViewById(R.id.textDate);
            TextView textQnt = findViewById(R.id.textQnt);
            TextView textExec = findViewById(R.id.textExec);
            TextView textProg = findViewById(R.id.textProg);
            TextView textObs = findViewById(R.id.textObs);

            Spinner textAtividade = findViewById(R.id.spinnerAtividade);
            Spinner textClima = findViewById(R.id.spinnerClima);

            Log.d(TAG, "Atividade => " + atividade);
            switch (atividade) {
                case "--Escolha uma atividade--":{
                    indexAtividade = 0;
                    break;
                }
                case "Implantação dos vértices e locação de torres ":{
                    indexAtividade = 1;
                    break;
                }
                case "Locação de torres e ponto critico":{
                    indexAtividade = 2;
                    break;
                }
                case "Locação de ponto central": {
                    indexAtividade = 3;
                    break;
                }
                case "Locação de diagonais e/ou PF":{
                    indexAtividade = 4;
                    break;
                }

                case "Implantação de vertices":{
                    indexAtividade = 5;
                    break;
                }
                case "Colocação de bandeiras":{
                    indexAtividade = 6;
                    break;
                }
                case "Integração": {
                    indexAtividade = 7;
                    break;
                }
                case "Sem atividades":{
                    indexAtividade = 8;
                    break;
                }
                case "Outros": {
                    indexAtividade = 9;
                    break;
                }
            }

            switch (condClimatica) {
                case "--Escolha o clima--":{
                    indexClima = 0;
                    break;
                }
                case "Bom": {
                    indexClima = 1;
                    break;
                }
                case "Chuva Dia Inteiro": {
                    indexClima = 3;
                    break;
                }
                case "Chuva Meio Periodo": {
                    indexClima = 2;
                    break;
                }
            }


            textDate.setText(date);
            if (indexAtividade != -1)
                textAtividade.setSelection(indexAtividade);
            else {
                textAtividade.setSelection(0);
                Log.d(TAG, "Atividade nao encontrada !");
            }

            if (indexClima != -1)
                textClima.setSelection(indexClima);
            else {
                textClima.setSelection(0);
                Log.d(TAG, "Clima nao encontrada !");
            }

            textQnt.setText(String.valueOf(qntTorres));
            textExec.setText(executado);
            textProg.setText(programaDiaSeguinte);
            textObs.setText(obs);

            textAtividade.setEnabled(false);
            textClima.setEnabled(false);
            textQnt.setEnabled(false);
            textExec.setEnabled(false);
            textProg.setEnabled(false);
            textObs.setEnabled(false);

        } else {
            TextView textDate = findViewById(R.id.textDate);
            TextView textQnt = findViewById(R.id.textQnt);
            TextView textExec = findViewById(R.id.textExec);
            TextView textProg = findViewById(R.id.textProg);
            TextView textObs = findViewById(R.id.textObs);
            Spinner textAtividade = findViewById(R.id.spinnerAtividade);
            Spinner textClima = findViewById(R.id.spinnerClima);

            textDate.setText(date);
            btnEdit.setVisibility(View.INVISIBLE);
            btnSalvar.setVisibility(View.VISIBLE);

        }

    }

    private void atualizaDados(int index,int type) {
        int condFinal = -1;

        String filePath = "/sdcard/Downloads/RELATORIO-DIARIO-DE-OBRA-ATOL.xlsx";
        File inputFile = new File(filePath);

        String value;
        String atividade, executado, progExecutado, obs, condClimatica, qntTorre;
        InputStream inputStream = null;
        XSSFWorkbook workbook = null;

        TextView textExec = findViewById(R.id.textExec);
        TextView textProg = findViewById(R.id.textProg);
        TextView textObs = findViewById(R.id.textObs);
        TextView textQnt = findViewById(R.id.textQnt);

        Spinner spinnerAtividade = findViewById(R.id.spinnerAtividade);
        Spinner spinnerClima = findViewById(R.id.spinnerClima);

        atividade = spinnerAtividade.getSelectedItem().toString();
        executado = textExec.getText().toString();
        progExecutado = textProg.getText().toString();
        obs = textObs.getText().toString();
        condClimatica = spinnerClima.getSelectedItem().toString();
        qntTorre = textQnt.getText().toString();

        int atividadeFinal = 0;
        //QntTorre tem que ser preenchido.
        try {

            inputStream = new FileInputStream(inputFile);
            workbook = new XSSFWorkbook(inputStream);
            XSSFSheet sheet = workbook.getSheetAt(0);
            FormulaEvaluator formulaEvaluator = workbook.getCreationHelper().createFormulaEvaluator();
            int rowsCount = sheet.getPhysicalNumberOfRows();
            int i=0;

            // Seleciona linha livre
            if ( type == 0 ) {
                for (i = 10; i < rowsCount; i += 2) {
                    Row row = sheet.getRow(i);
                    value = getCellAsString(row, 0, formulaEvaluator);
                    if (value.equals("")) {
                        break;
                    }
                }
            }

            String dataSelect = MainActivity.dados.get(index).date;
            if ( type == 1 ){
                    for (i = 10; i < rowsCount; i += 2) {
                    Row row = sheet.getRow(i);
                    value = getCellAsString(row, 0, formulaEvaluator);
                    if (value.equals(dataSelect)) {
                        break;
                    }
                }
            }

            Row row1 = sheet.getRow(i);
            Row row2 = sheet.getRow(i + 1);

            int tot = 0;
            for ( int j = 0 ; j < MainActivity.dados.size(); j++ ){
                tot += MainActivity.dados.get(j).qntTorres;
                Log.d(TAG,"TOTAL TORRES ["+j+"] = "+MainActivity.dados.get(j).qntTorres);
                Log.d(TAG,"TOT = "+tot);
            }


            final Row rowTopografo = sheet.getRow(7);
            Cell cellQnt = rowTopografo.getCell(6);
            cellQnt.setCellValue("TOTAL Torres Locadas = "+tot);

           Date date1=new SimpleDateFormat("dd/MM/yyyy").parse(date);

            for (int j = 0; j < 7; j++) {
                Cell cell = row1.getCell(j);
                Cell cell2 = row2.getCell(j);
                switch (j) {
                    case 0: {
                        cell.setCellValue(date);
                        cell2.setCellValue(new SimpleDateFormat("EEEE").format(date1));
                        break;
                    }
                    case 1: {
                        cell.setCellValue(atividade);
                        break;
                    }

                    case 2: {
                        if (!qntTorre.isEmpty())
                            cell.setCellValue(Integer.parseInt(qntTorre));
                        else
                            cell.setCellValue(0);

                        break;
                    }
                    case 3: {
                        cell.setCellValue(executado);
                        break;
                    }
                    case 4: {
                        cell.setCellValue(progExecutado);
                        break;
                    }
                    case 5: {
                        cell.setCellValue(condClimatica);

                        break;
                    }
                    case 6: {
                        cell.setCellValue(obs);
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }

            FileOutputStream os = new FileOutputStream("/sdcard/Downloads/RELATORIO-DIARIO-DE-OBRA-ATOL.xlsx");
            workbook.write(os);
            workbook.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }


    private String getCellAsString(Row row, int c, FormulaEvaluator formulaEvaluator) {
         String value = "";
        try {
            Cell cell = row.getCell(c);
            CellValue cellValue = formulaEvaluator.evaluate(cell);
            switch (cellValue.getCellType()) {
                case Cell.CELL_TYPE_BOOLEAN:
                    value = ""+cellValue.getBooleanValue();
                    break;
                case Cell.CELL_TYPE_NUMERIC:
                    double numericValue = cellValue.getNumberValue();
                    if(HSSFDateUtil.isCellDateFormatted(cell)) {
                        double date = cellValue.getNumberValue();
                        SimpleDateFormat formatter =
                                new SimpleDateFormat("dd/MM/yyyy");
                        value = formatter.format(HSSFDateUtil.getJavaDate(date));
                    } else {
                        value = ""+numericValue;
                    }
                    break;
                case Cell.CELL_TYPE_STRING:
                    value = ""+cellValue.getStringValue();
                    break;
                default:
            }
        } catch (NullPointerException e) {

            Log.e(TAG, "getCellAsString: NullPointerException: " + e.getMessage() );
        }
        return value;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home) {
            this.finish();
        }
        return super.onOptionsItemSelected(item);

    }
}


