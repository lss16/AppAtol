package com.example.atolapp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

public class SplashScreen extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splashscreen);
             new Handler().postDelayed(new Runnable() {
                    /*
                     * Exibindo splash com um timer.
                     */
                    @Override
                    public void run() {
                           // Esse método será executado sempre que o timer acabar
                           // E inicia a activity principal
                           Intent i = new Intent(SplashScreen.this,
                           MainActivity.class);
                           startActivity(i);

                           // Fecha esta activity
                           finish();
                    }
             }, SPLASH_TIME_OUT);
    }

}
