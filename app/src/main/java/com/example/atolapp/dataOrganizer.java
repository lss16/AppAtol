package com.example.atolapp;

public class dataOrganizer
{

    public int qntTorres;
    public String date,atividade,executado,progExec,condClimatica,obs;

    public dataOrganizer(String date, String atividade,int qntTorres,
                         String executado, String progExec,String condClimatica, String obs){

        this.date = date;
        this.atividade = atividade;
        this.qntTorres = qntTorres;
        this.executado = executado;
        this.progExec = progExec;
        this.condClimatica = condClimatica;
        this.obs = obs;

    }

}
